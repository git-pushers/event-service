package v1

import (
	"context"
	"encoding/json"
	"io"
	"log/slog"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"

	"gitlab.com/git-pushers/event-service/internal/domain"
	"gitlab.com/git-pushers/event-service/internal/dto"
	"gitlab.com/git-pushers/event-service/internal/handler/http/v1/model"
	"gitlab.com/git-pushers/event-service/pkg/resp"
)

type iUC interface {
	CreateEvent(ctx context.Context, event dto.Event) (domain.Event, error)
	GetEvents(ctx context.Context, filter dto.Filter, pagination dto.Pagination) ([]domain.Event, bool, error)
}

type Handler struct {
	uc iUC
}

func New(uc iUC, r chi.Router) Handler {
	h := Handler{uc: uc}

	r.Route("/v1", func(r chi.Router) {
		r.Get("/events", h.GetEvents)
		r.Post("/events", h.PostEvent)
	})
	return h
}

// PostEvent godoc
// @Tags Event
// @Summary creates event
// @Produce json
// @Param PostEventReq body model.PostEventReq true "Fields of event to be created"
// @Success 200 {object} model.PostEventResp
// @Router /v1/events [post]
func (h Handler) PostEvent(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	data, err := io.ReadAll(r.Body)
	if err != nil {
		slog.With("err", err.Error()).Error("failed to read body")
		render.Render(w, r, resp.Unknown(err, ""))
		return
	}
	var event model.PostEventReq
	if err = json.Unmarshal(data, &event); err != nil {
		slog.With("err", err.Error()).Error("failed to unmarshal req")
		render.Render(w, r, resp.BadRequest(err, ""))
		return
	}

	createdEvent, err := h.uc.CreateEvent(ctx, event.ToDTO())
	if err != nil {
		slog.With("err", err.Error()).Error("failed to create event")
		render.Render(w, r, resp.BadRequest(err, ""))
		return
	}

	render.Render(w, r, resp.OK(model.ToPostEventResp(createdEvent), ""))
}

// GetEvents godoc
// @Tags Event
// @Summary get events
// @Produce json
// @Param query query string true "query to search event"
// @Param is_private query bool true "query to select private events"
// @Param company_ids query []int true "company ids to filter events"
// @Param offset query int true "offset to filter events"
// @Param limit query int true "limit to filter events"
// @Success 200 {object} model.GetEventsResp
// @Router /v1/events [get]
func (h Handler) GetEvents(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	req, err := model.FilterAndPaginationQuery(r)
	if err != nil {
		slog.With("err", err.Error()).Error("get query")
		render.Render(w, r, resp.BadRequest(err, ""))
		return
	}

	events, hasNext, err := h.uc.GetEvents(ctx, req.GetFilter(), req.GetPagination())
	if err != nil {
		slog.With("err", err.Error()).Error("failed to get events")
		render.Render(w, r, resp.BadRequest(err, ""))
		return
	}

	render.Render(w, r, resp.OK(model.NewGetEventsResp(events, hasNext), ""))
}
