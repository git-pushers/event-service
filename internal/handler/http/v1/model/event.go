package model

import (
	"net/http"
	"time"

	"github.com/gorilla/schema"
	"github.com/pkg/errors"

	"gitlab.com/git-pushers/event-service/internal/domain"
	"gitlab.com/git-pushers/event-service/internal/dto"
)

type PostEventReq struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Location    string `json:"location"`
	Time        string `json:"time"`
	Duration    string `json:"duration"`
	IsFree      bool   `json:"is_free"`
	IsPrivate   bool   `json:"is_private"`
	EventStatus string `json:"status"`
}

func (resp PostEventReq) ToDTO() dto.Event {
	return dto.Event{
		Name:        resp.Name,
		Description: resp.Description,
		Location:    resp.Location,
		Time:        resp.Time,
		Duration:    resp.Duration,
		IsFree:      resp.IsFree,
		IsPrivate:   resp.IsPrivate,
		EventStatus: resp.EventStatus,
	}
}

type PostEventResp struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Location    string `json:"location"`
	Time        string `json:"time"`
	Duration    string `json:"duration"`
	IsFree      bool   `json:"is_free"`
	IsPrivate   bool   `json:"is_private"`
	EventStatus string `json:"status"`
}

func ToPostEventResp(event domain.Event) PostEventResp {
	return PostEventResp{
		Name:        event.Name,
		Description: event.Description,
		Location:    event.Location,
		Time:        event.Time,
		Duration:    event.Duration,
		IsFree:      event.IsFree,
		IsPrivate:   event.IsPrivate,
		EventStatus: event.EventStatus,
	}
}

type GetEventsReq struct {
	Query      string  `schema:"query"`
	IsPrivate  bool    `schema:"is_private"`
	CompanyIDs []int64 `schema:"company_ids"`
	Offset     int64   `schema:"offset"`
	Limit      int64   `schema:"limit"`
}

func (req GetEventsReq) GetPagination() dto.Pagination {
	return dto.Pagination{
		Offset: req.Offset,
		Limit:  req.Limit,
	}
}

func (req GetEventsReq) GetFilter() dto.Filter {
	return dto.Filter{
		Query:      req.Query,
		IsPrivate:  req.IsPrivate,
		CompanyIDs: req.CompanyIDs,
	}
}

func FilterAndPaginationQuery(r *http.Request) (GetEventsReq, error) {
	if err := r.ParseForm(); err != nil {
		return GetEventsReq{}, err
	}
	var query GetEventsReq
	if err := schema.NewDecoder().Decode(&query, r.Form); err != nil {
		return GetEventsReq{}, errors.Wrap(err, "decode")
	}

	return query, nil
}

type GetEventsResp struct {
	Events  []Event `json:"events"`
	HasNext bool    `json:"has_next"`
}

func NewGetEventsResp(events []domain.Event, hasNext bool) GetEventsResp {
	modelEvents := make([]Event, 0, len(events))
	for _, event := range events {
		modelEvents = append(modelEvents, ToEvent(event))
	}

	return GetEventsResp{
		Events:  modelEvents,
		HasNext: hasNext,
	}
}

type Event struct {
	ID          int64     `json:"id"`
	CompanyID   int64     `json:"company_id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Location    string    `json:"location"`
	Time        string    `json:"time"`
	Duration    string    `json:"duration"`
	IsFree      bool      `json:"is_free"`
	IsPrivate   bool      `json:"is_private"`
	EventStatus string    `json:"status"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

func ToEvent(event domain.Event) Event {
	return Event{
		Name:        event.Name,
		Description: event.Description,
		Location:    event.Location,
		Time:        event.Time,
		Duration:    event.Duration,
		IsFree:      event.IsFree,
		IsPrivate:   event.IsPrivate,
		EventStatus: event.EventStatus,
	}
}
