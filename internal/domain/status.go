package domain

type EventStatus int

const (
	// TODO: exclude zero to avoid default values

	New       EventStatus = 0
	Publish   EventStatus = 1
	Hidden    EventStatus = 2
	Cancelled EventStatus = 3
)
