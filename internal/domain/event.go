package domain

import "time"

type Event struct {
	ID          int64
	CompanyID   int64
	Name        string
	Description string
	Location    string
	Time        string
	Duration    string
	IsFree      bool
	IsPrivate   bool
	EventStatus string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
