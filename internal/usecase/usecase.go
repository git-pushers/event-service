package usecase

import (
	"context"

	"gitlab.com/git-pushers/event-service/internal/domain"
	"gitlab.com/git-pushers/event-service/internal/dto"
)

type iManticoreRepo interface {
	CreateEvent(ctx context.Context, event dto.Event) (domain.Event, error)
	GetEventsByCompanyID(ctx context.Context, filter dto.Filter, pagination dto.Pagination) ([]domain.Event, bool, error)
}

type UC struct {
	repo iManticoreRepo
}

func New(repo iManticoreRepo) UC {
	return UC{repo: repo}
}

func (uc UC) CreateEvent(ctx context.Context, event dto.Event) (domain.Event, error) {
	return uc.repo.CreateEvent(ctx, event)
}

func (uc UC) GetEvents(
	ctx context.Context,
	filter dto.Filter,
	pagination dto.Pagination,
) ([]domain.Event, bool, error) {
	return uc.repo.GetEventsByCompanyID(ctx, filter, pagination)
}
