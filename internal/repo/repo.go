package repo

import (
	"context"
	"math/rand"
	"time"

	"gitlab.com/git-pushers/event-service/internal/domain"
	"gitlab.com/git-pushers/event-service/internal/dto"
)

type Repo struct {
}

func New() Repo {
	return Repo{}
}

func (r Repo) CreateEvent(ctx context.Context, event dto.Event) (domain.Event, error) {
	return domain.Event{
		ID:          rand.Int63(),
		CompanyID:   rand.Int63(),
		Name:        event.Name,
		Description: event.Description,
		Location:    event.Location,
		Time:        event.Time,
		Duration:    event.Duration,
		IsFree:      event.IsFree,
		IsPrivate:   event.IsPrivate,
		EventStatus: event.EventStatus,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}, nil
}

func (r Repo) GetEventsByCompanyID(
	ctx context.Context,
	filter dto.Filter,
	pagination dto.Pagination,
) ([]domain.Event, bool, error) {
	return []domain.Event{
			{
				ID:          rand.Int63(),
				CompanyID:   filter.CompanyIDs[0],
				Name:        "name",
				Description: "description",
				Location:    "location",
				Time:        "time",
				Duration:    "duration",
				IsFree:      false,
				IsPrivate:   true,
				EventStatus: "new",
				CreatedAt:   time.Now(),
				UpdatedAt:   time.Now(),
			},
		},
		false,
		nil
}
