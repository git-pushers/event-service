package config

import (
	"fmt"
	"log/slog"
	"os"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/pkg/errors"

	"gitlab.com/git-pushers/event-service/pkg/http"
)

const (
	configPathEnvName     = "CONFIG_PATH"
	defaultConfigYAMLPath = "./config/application.yaml"
)

type Cfg struct {
	Host        string            `yaml:"host"`
	Srv         http.ServerConfig `yaml:"srv"`
	GraylogHost string            `yaml:"graylog_host"`
}

func Setup() (*Cfg, error) {
	configPath := os.Getenv(configPathEnvName)
	if configPath == "" {
		configPath = defaultConfigYAMLPath
	}
	var cfg Cfg
	if err := cleanenv.ReadConfig(configPath, &cfg); err != nil {
		return nil, errors.Wrap(err, "failed to read application config")
	}

	return &cfg, nil
}

func (cfg Cfg) PrintLocalURLs() {
	slog.Info(fmt.Sprintf("SWAGGER: http://localhost:%d/swagger/index.html", cfg.Srv.Port))
}
