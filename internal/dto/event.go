package dto

type Event struct {
	Name        string
	Description string
	Location    string
	Time        string
	Duration    string
	IsFree      bool
	IsPrivate   bool
	EventStatus string
}

type Filter struct {
	Query      string
	IsPrivate  bool
	CompanyIDs []int64
}
