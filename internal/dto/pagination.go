package dto

type Pagination struct {
	Offset int64
	Limit  int64
}
