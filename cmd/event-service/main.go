package main

import (
	"io"
	"log"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/Graylog2/go-gelf/gelf"
	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	httpSwagger "github.com/swaggo/http-swagger/v2"
	jaeger "github.com/uber/jaeger-client-go/config"

	"gitlab.com/git-pushers/event-service/docs"
	"gitlab.com/git-pushers/event-service/internal/config"
	httpv1 "gitlab.com/git-pushers/event-service/internal/handler/http/v1"
	"gitlab.com/git-pushers/event-service/internal/repo"
	"gitlab.com/git-pushers/event-service/internal/usecase"
	"gitlab.com/git-pushers/event-service/pkg/http"
)

// @title event-service Swagger API
// @version 1.0
// @description Swagger API for Project event-service.
func main() {
	defer doRecover()

	cfg, err := config.Setup()
	if err != nil {
		panic(errors.Wrap(err, "failed to setup config"))
	}

	// Tracing: Jaeger
	tracer, closer, err := jaegerConfig().New("event-service") // nolint
	if err != nil {
		log.Fatalf("jaeger error - %s", err.Error())
	}
	opentracing.SetGlobalTracer(tracer)
	defer closer.Close()

	// Logger: Graylog
	slog.SetDefault(logger(cfg.GraylogHost))

	srv := http.NewServer(cfg.Srv)
	httpErrCh := srv.Start()
	defer func() {
		if err := srv.Stop(); err != nil {
			slog.Error("failed to stop http server", "err", err.Error())
		}
	}()
	repo := repo.New()
	uc := usecase.New(repo)
	_ = httpv1.New(uc, srv.Mux)

	slog.Info("event-service was started")

	docs.SwaggerInfo.Host = cfg.Host
	docs.SwaggerInfo.BasePath = "/"
	srv.Mux.Get("/swagger/*", httpSwagger.WrapHandler)

	cfg.PrintLocalURLs()

	// on stop section
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)
	// wait for stop signal
	select {
	case <-signals:
	case err := <-httpErrCh:
		panic(errors.Wrap(err, "http server"))
	}
}

func doRecover() {
	defer func() {
		if recErr := recover(); recErr != nil {
			switch v := recErr.(type) {
			case error, string:
				slog.Error("fatal error", "err", v)
			default:
				slog.Error("unsupported recover type", "type", v)
			}
			os.Exit(1)
		}
	}()
}

func logger(host string) *slog.Logger {
	gelfWriter, err := gelf.NewWriter(host)
	if err != nil {
		log.Fatalf("gelf.NewWriter: %s", err)
	}

	return slog.New(slog.NewJSONHandler(io.MultiWriter(os.Stdout, gelfWriter), nil))
}

func jaegerConfig() jaeger.Configuration {
	return jaeger.Configuration{
		Sampler: &jaeger.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &jaeger.ReporterConfig{
			LogSpans:            false,
			BufferFlushInterval: 1 * time.Second,
		},
	}
}
