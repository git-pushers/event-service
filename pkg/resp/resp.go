package resp

type ResultCode string

const (
	OKCode    ResultCode = "OK"
	ErrorCode ResultCode = "ERROR"
)
