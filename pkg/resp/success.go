package resp

import (
	"net/http"

	"github.com/go-chi/render"
)

type Success struct {
	ResultCode ResultCode  `json:"resultCode"`
	TrackingID string      `json:"trackingId"`
	Payload    interface{} `json:"payload,omitempty"`
	statusCode int
}

func (s Success) Render(_ http.ResponseWriter, r *http.Request) error {
	render.Status(r, s.statusCode)
	return nil
}

func OK(payload interface{}, traceID string) *Success {
	return &Success{
		ResultCode: OKCode,
		TrackingID: traceID,
		Payload:    payload,
		statusCode: http.StatusOK,
	}
}

func Empty(traceID string) *Success {
	return &Success{
		ResultCode: OKCode,
		TrackingID: traceID,
		Payload:    nil,
		statusCode: http.StatusOK,
	}
}
