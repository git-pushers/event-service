package resp

import (
	"net/http"

	"github.com/go-chi/render"
)

type Error struct {
	ResultCode ResultCode `json:"resultCode"`
	TrackingID string     `json:"trackingId"`
	ErrorMsg   string     `json:"errorMessage"`
	statusCode int
}

func (e Error) Render(_ http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.statusCode)
	return nil
}

func Unknown(err error, traceID string) *Error {
	return &Error{
		ResultCode: ErrorCode,
		ErrorMsg:   err.Error(),
		TrackingID: traceID,
		statusCode: http.StatusInternalServerError,
	}
}

func NotFound(err error, traceID string) *Error {
	return &Error{
		ResultCode: ErrorCode,
		ErrorMsg:   err.Error(),
		TrackingID: traceID,
		statusCode: http.StatusNotFound,
	}
}

func BadRequest(err error, traceID string) *Error {
	return &Error{
		ResultCode: ErrorCode,
		ErrorMsg:   err.Error(),
		TrackingID: traceID,
		statusCode: http.StatusBadRequest,
	}
}
