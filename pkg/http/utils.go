package http

import "fmt"

func MakeAddr(port int) string {
	return fmt.Sprintf(":%d", port)
}
