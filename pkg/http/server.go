package http

import (
	"net/http"
	_ "net/http/pprof"

	"github.com/go-chi/chi/v5"
	"github.com/pkg/errors"
)

type Server struct {
	Mux chi.Router
	srv *http.Server
	cfg ServerConfig
}

// NewServer - устанавливает дефолтные настройки порта и таймаутов, применяет встроенные и переданные middleware
func NewServer(cfg ServerConfig, middlewares ...func(http.Handler) http.Handler) *Server {
	cfg.SetDefaults()

	mux := chi.NewRouter()

	if middlewares != nil {
		mux.Use(middlewares...)
	}

	return &Server{
		cfg: cfg,
		Mux: mux,
		srv: &http.Server{
			Addr:         MakeAddr(cfg.Port),
			Handler:      mux,
			ReadTimeout:  cfg.ReadTimeout,
			WriteTimeout: cfg.WriteTimeout,
		},
	}
}

func (s *Server) Start() <-chan error {
	defaultRoutes := GetDefaultRoutes(s.cfg)
	for _, route := range defaultRoutes {
		s.Mux.Method(route.Method, route.Path, http.HandlerFunc(route.Handler))
	}

	errCh := make(chan error)
	go func() {
		errCh <- errors.WithStack(s.srv.ListenAndServe())
		close(errCh)
	}()
	return errCh
}

func (s *Server) Stop() error {
	return s.srv.Close()
}

func (s *Server) GetPort() int {
	return s.cfg.Port
}
