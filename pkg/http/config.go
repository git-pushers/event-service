package http

import "time"

const (
	defaultPort         = 9090
	defaultReadTimeout  = 5 * time.Second
	defaultWriteTimeout = 5 * time.Second
)

type ServerConfig struct {
	Port                int           `yaml:"port" env:"PORT"`
	ReadTimeout         time.Duration `yaml:"read_timeout" env:"READ_TIMEOUT"`
	WriteTimeout        time.Duration `yaml:"write_timeout" env:"WRITE_TIMEOUT"`
	ProfileEnabled      bool          `yaml:"profile_enabled" env:"PROFILE_ENABLED"`
	DefaultRoutesPrefix string        `yaml:"default_routes_prefix" env:"DEFAULT_ROUTES_PREFIX"`
	Swagger             SwaggerConfig `yaml:"swagger" env:"SWAGGER"`
}

type SwaggerConfig struct {
	Enabled  bool   `mapstructure:"enabled" yaml:"enabled" env:"ENABLED"`
	Host     string `mapstructure:"host" yaml:"host" env:"HOST"`
	BasePath string `mapstructure:"base_path" yaml:"base_path" env:"BASE_PATH"`
	Scheme   string `mapstructure:"scheme" yaml:"scheme" env:"SCHEME"`
}

func (c *ServerConfig) SetDefaults() {
	if c.Port == 0 {
		c.Port = defaultPort
	}
	if c.ReadTimeout == 0 {
		c.ReadTimeout = defaultReadTimeout
	}
	if c.WriteTimeout == 0 {
		c.WriteTimeout = defaultWriteTimeout
	}
}
