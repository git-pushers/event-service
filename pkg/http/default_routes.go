package http

import (
	"fmt"
	"net/http"
	"path"

	httpSwagger "github.com/swaggo/http-swagger/v2"
)

type Route struct {
	Method  string
	Path    string
	Handler func(http.ResponseWriter, *http.Request)
}

func routeGet(
	cfg ServerConfig,
	methodPath string,
	methodHandler func(http.ResponseWriter, *http.Request),
) Route {
	return Route{
		Method:  http.MethodGet,
		Path:    path.Join(cfg.DefaultRoutesPrefix, methodPath),
		Handler: methodHandler,
	}
}

func GetDefaultRoutes(cfg ServerConfig) []Route {
	res := []Route{
		routeGet(cfg, "/healthcheck", healthcheck),
	}

	if cfg.Swagger.Enabled {
		res = append(res, routeGet(cfg, "/swagger/*", httpSwagger.WrapHandler.ServeHTTP))
	}

	if cfg.ProfileEnabled {
		res = append(res, routeGet(cfg, "/debug/pprof/*", http.DefaultServeMux.ServeHTTP))
	}

	return res
}

func healthcheck(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "ok")
}
