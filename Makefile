PACKAGE = event-service

BIN = $(PWD)/bin
BUILD = $(PWD)/bin/build/$(PACKAGE)
MAIN = $(PWD)/cmd/$(PACKAGE)

GOLANGCI_LINT_VER = v1.55.2
SWAGGO_VER = v1.8.12

# load environment variables from .env file
include .env
export

.env:
	touch $@

.PHONY: all
all: build lint

bin/golangci-lint:
	GOBIN=$(BIN) go install github.com/golangci/golangci-lint/cmd/golangci-lint@$(GOLANGCI_LINT_VER)

bin/swag:
	GOBIN=$(BIN) go install github.com/swaggo/swag/cmd/swag@$(SWAGGO_VER)

.PHONY: lint
lint: bin/golangci-lint
	$(BIN)/golangci-lint run ./...

.PHONY: swag
swag: bin/swag
	$(BIN)/swag init --parseDependency=true --parseGoList=false --parseDepth 1 -g /cmd/event-service/main.go

.PHONY: build
build:
	go build -tags musl -mod=vendor -o ${BUILD} ${MAIN}

.PHONY: run
run: build
	${BUILD}
