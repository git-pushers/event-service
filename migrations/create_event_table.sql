CREATE TABLE event (
    event_at integer,
    event_name string,
    event_location string,
    event_photo string,
    is_free bool,
    is_private bool,
    event_status string
    );